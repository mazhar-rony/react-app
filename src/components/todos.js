import axios from "axios";
import Table from "../common/table.component";
import { useEffect, useState } from "react";
import _ from "lodash";

const Todos = () => {
    const [todos, setTodos] = useState([]);

    const getTodos = () => {
        const success = (response) => {
            const todos = response.data.todos;
            setTodos(todos);
        }

        const error = (err) => {
            console.log(err);
        }

        axios.get("https://dummyjson.com/todos").then(success).catch(error);
    }

    useEffect(getTodos, []);

    const columns = [
        {
          label: "ID",
          key: "id",
          isSortable: true,
        },
        {
          label: "Todo",
          key: "todo",
          isSortable: true,
        },
        {
          label: "Is Completed",
          // key: "completed",
          render: (item) => {
            return (
              <td key={item.label}>{item.completed ? <span className=" btn btn-primary">Completed</span> : <span className=" btn btn-danger">Pending</span>}</td>
            );
          },
        },
      ];

    const [sortKey, setSortKey] = useState("id");
    const [sortOrder, setSortOrder] = useState("asc");

    const handleSort = (newSortKey) => {
      if (sortKey === newSortKey) {
        if (sortOrder === "asc") {
          setSortOrder("desc");
        } else {
          setSortOrder("asc");
        }
      } else {
        setSortKey(newSortKey);
        setSortOrder("asc");
      }
    };

    const sortedTodos = _.orderBy(todos, sortKey, sortOrder);

    return ( 
      <div className="container mt-5">
          <Table items={sortedTodos} columns={columns} sortKey={sortKey}
                sortOrder={sortOrder} handleSort={handleSort} />
      </div>
    );
}
 
export default Todos;