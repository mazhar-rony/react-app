// import { string, object } from "yup";
import * as Yup from "yup";

// export const registrationSchema = object().shape({
//   email: string()
//     .required("This field cannot be empty.")
//     .email("This field must be an valid email address"),
//   firstName: string().required("This field cannot be empty."),
//   lastName: string().required("This field cannot be empty."),
//   username: string().required("This field cannot be empty."),
//   phone: string().required("This field cannot be empty."),
//   sex: string().required("A radio option is required"),
//   password: string().required("Password is required"),
//   passwordConfirmation: string().oneOf(["password"], "Passwords must match"),
// });

export const registrationSchema = Yup.object({
  email: Yup.string()
    .required("This field cannot be empty.")
    .email("This field must be an valid email address"),
  firstName: Yup.string().required("This field cannot be empty."),
  lastName: Yup.string().required("This field cannot be empty."),
  username: Yup.string().required("This field cannot be empty."),
  phone: Yup.string().required("This field cannot be empty."),
  sex: Yup.string().required("A radio option is required"),
  password: Yup.string().required("Password is required").min(4, "Password must be at least 4 characters"),
  confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], "Passwords must match").required("Confirm Password is required"),
});
