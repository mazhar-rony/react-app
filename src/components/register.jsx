import { Formik, Form, Field, ErrorMessage } from "formik";
import axios from "axios";
import { registrationSchema } from "./user.schema";

const Register = () => {
  const handleSubmit = async (
    email,
    firstName,
    lastName,
    username,
    phone,
    sex,
    password,
    confirmPassword
  ) => {
    try {
      const body = {
        email,
        firstName,
        lastName,
        username,
        phone,
        sex,
        password,
        confirmPassword,
      };

      const response = await axios.post("https://dummyjson.com/auth/login",body);
      console.log(response.data);
      alert(`Login SUccessfull! \nName: ${response.data.firstName} ${response.data.lastName}`);
    } catch (err) {
      console.log(err);
      alert(err.message);
    }
  };

  return (
    <div className="container">
      <div className="col-lg-6 ms-auto me-auto">
        <Formik
          initialValues={{
            email: "",
            firstName: "",
            lastName: "",
            username: "",
            phone: "",
            sex: "male",
            password: "",
            confirmPassword: "",
          }}
          onSubmit={(values, actions) => {
            console.log(values);
            handleSubmit(
              values.email,
              values.firstName,
              values.lastName,
              values.username,
              values.phone,
              values.sex,
              values.password,
              values.confirmPassword
            );
            actions.setSubmitting(false);
          }}
          validationSchema={registrationSchema}
        >
          {(formikProps) => {
            return (
              <Form onSubmit={formikProps.handleSubmit}>
                <div className="form-group row mt-2">
                  <label
                    htmlFor="inputEmail"
                    className="col-sm-3 col-form-label"
                  >
                    Email
                  </label>
                  <div className="col-sm-9">
                    <Field
                      type="email"
                      className="form-control"
                      id="inputEmail"
                      name="email"
                      placeholder="Email"
                    />
                  </div>
                  <div className="invalid-feedback d-flex offset-md-3">
                    <ErrorMessage name="email" />
                  </div>
                </div>
                <div className="form-group row mt-2">
                  <label
                    htmlFor="inputFirstName"
                    className="col-sm-3 col-form-label"
                  >
                    First Name
                  </label>
                  <div className="col-sm-9">
                    <Field
                      type="text"
                      className="form-control"
                      id="inputFirstName"
                      name="firstName"
                      placeholder="First Name"
                    />
                  </div>
                  <div className="invalid-feedback d-flex offset-md-3">
                    <ErrorMessage name="firstName" />
                  </div>
                </div>
                <div className="form-group row mt-2">
                  <label
                    htmlFor="inputLastName"
                    className="col-sm-3 col-form-label"
                  >
                    Last Name
                  </label>
                  <div className="col-sm-9">
                    <Field
                      type="text"
                      className="form-control"
                      id="inputLastName"
                      name="lastName"
                      placeholder="Last Name"
                    />
                  </div>
                  <div className="invalid-feedback d-flex offset-md-3">
                    <ErrorMessage name="lastName" />
                  </div>
                </div>
                <div className="form-group row mt-2">
                  <label
                    htmlFor="inputUserName"
                    className="col-sm-3 col-form-label"
                  >
                    User Name
                  </label>
                  <div className="col-sm-9">
                    <Field
                      type="text"
                      className="form-control"
                      id="inputUserName"
                      name="username"
                      placeholder="User Name"
                    />
                  </div>
                  <div className="invalid-feedback d-flex offset-md-3">
                    <ErrorMessage name="username" />
                  </div>
                </div>
                <div className="form-group row mt-2">
                  <label
                    htmlFor="inpuPhone"
                    className="col-sm-3 col-form-label"
                  >
                    Phone
                  </label>
                  <div className="col-sm-9">
                    <Field
                      type="text"
                      className="form-control"
                      id="inpuPhone"
                      name="phone"
                      placeholder="Phone"
                    />
                  </div>
                  <div className="invalid-feedback d-flex offset-md-3">
                    <ErrorMessage name="phone" />
                  </div>
                </div>
                <fieldset className="form-group mt-2">
                  <div className="row">
                    <legend className="col-form-label col-sm-3 pt-0">
                      Radios
                    </legend>
                    <div className="col-sm-9">
                      <div className="form-check">
                        <Field
                          className="form-check-input"
                          type="radio"
                          name="sex"
                          id="male"
                          value="male"
                        />
                        <label className="form-check-label" htmlFor="male">
                          Male
                        </label>
                      </div>
                      <div className="form-check">
                        <Field
                          className="form-check-input"
                          type="radio"
                          name="sex"
                          id="female"
                          value="female"
                        />
                        <label className="form-check-label" htmlFor="female">
                          Female
                        </label>
                      </div>
                      <div className="form-check">
                        <Field
                          className="form-check-input"
                          type="radio"
                          name="sex"
                          id="other"
                          value="other"
                        />
                        <label className="form-check-label" htmlFor="other">
                          Other
                        </label>
                      </div>
                    </div>
                    <div className="invalid-feedback d-flex offset-md-3">
                      <ErrorMessage name="sex" />
                    </div>
                  </div>
                </fieldset>
                <div className="form-group row mt-2">
                  <label
                    htmlFor="inputPassword"
                    className="col-sm-3 col-form-label"
                  >
                    Password
                  </label>
                  <div className="col-sm-9">
                    <Field
                      type="password"
                      className="form-control"
                      id="inputPassword"
                      name="password"
                      placeholder="Password"
                    />
                  </div>
                  <div className="invalid-feedback d-flex offset-md-3">
                    <ErrorMessage name="password" />
                  </div>
                </div>
                <div className="form-group row mt-2">
                  <label
                    htmlFor="inputConfirmPassword"
                    className="col-sm-3 col-form-label"
                  >
                    Confirm Password
                  </label>
                  <div className="col-sm-9">
                    <Field
                      type="password"
                      className="form-control"
                      id="inputConfirmPassword"
                      name="confirmPassword"
                      placeholder="Confirm Password"
                    />
                  </div>
                  <div className="invalid-feedback d-flex offset-md-3">
                    <ErrorMessage name="confirmPassword" />
                  </div>
                </div>
                <div className="form-group row mt-2">
                  <div className="col-sm-9 offset-md-3">
                    <button type="submit" className="btn btn-primary">
                      Sign up
                    </button>
                  </div>
                </div>
              </Form>
            );
          }}
        </Formik>
        <form></form>
      </div>
    </div>
  );
};

export default Register;
