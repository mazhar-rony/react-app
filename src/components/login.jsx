import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function Login () {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    // const [user, setUser] = useState(null);

    const navigate = useNavigate();

    const handleLogin = (event) => {
      event.preventDefault();

      const success = (response) => {
        // setUser(response.data);
        localStorage.setItem("loggedInUser", true);

        navigate("/counter");
      }

      const error = (err) => {
        console.log(err);
        alert(err.message);
      }

      const body = {
        username: email,
        password
      };

      axios.post('https://dummyjson.com/auth/login', body)
        .then(success)
        .catch(error)
    }

   
    return (
          <div className="container">
            <div className="col-lg-6 ms-auto me-auto">
              <form>
                <div className="form-group mt-5">
                  <label htmlFor="email">Email address</label>
                  <input type="email" className="form-control" value={email} id="email" placeholder="Enter email" 
                    onChange={e => setEmail(e.target.value)} autoComplete='off'/>
                </div>
                <div className="form-group mt-2">
                  <label htmlFor="password">Password</label>
                  <input type="password" className="form-control" value={password} id="password" placeholder="Password" 
                    onChange={e => setPassword(e.target.value)}/>
                </div>
                <button type="button" className="btn btn-primary mt-3"
                  onClick={handleLogin}>Login</button>
              </form>
            </div>
          </div>
    );
}
 
export default Login;