import React, { Component } from 'react';
import { Navigate } from 'react-router-dom';

class Counter extends Component {
    state = { 
        counter: 50
    };
     
    increment = () => {
        this.state.counter++;

        const newState = {
            counter: this.state.counter
        };

        this.setState(newState);

        //this.setState({counter: this.state.counter++});
    }

    decrement = () => {
        this.state.counter--;

        const newState = {
            counter: this.state.counter
        };

        this.setState(newState);

        //this.setState({counter: this.state.counter--});
    }


    render() { 
        const loggedInUser = localStorage.getItem("loggedInUser");
        return (
            <>
                <div className="container">
                    <div className="card mt-5 ms-5" style={{width: "11rem"}}>
                    {!loggedInUser && <Navigate to="/login" replace={true}></Navigate>}
                    <div className="card-body">              
                        <button className='btn btn-success btn-lg me-2' onClick={this.increment}>+</button> 
                        <span className='badge rounded-pill bg-info'>{this.state.counter}</span> 
                        <button className='btn btn-danger btn-lg ms-2' onClick={this.decrement}>-</button>
                    </div>
                    </div>
                </div>
            </>
        );
    }
}
 
export default Counter;