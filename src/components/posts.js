import axios from "axios";
import { useEffect, useState } from "react";
import Card from "../common/card.component";

const Posts = () => {
  const [posts, setPosts] = useState([]);
  const [comments, setComment] = useState([]);
  const [users, setUsers] = useState([]);

  const getPosts = () => {
    const success = (response) => {
      const posts = response.data.posts;
      // console.log(posts);
      setPosts(posts);
    };
    const error = (err) => {
      console.log(err);
    };

    axios
      .get("https://dummyjson.com/posts?limit=150")
      .then(success)
      .then(error);
  };

  const getComments = () => {
    const success = (response) => {
      const comments = response.data.comments;
      // console.log(comments);
      setComment(comments);
    };
    const error = (err) => {
      console.log(err);
    };

    axios
      .get("https://dummyjson.com/comments?limit=340")
      .then(success)
      .then(error);
  };

  const getUsers = () => {
    const success = (response) => {
      const users = response.data.users;
      // console.log(users);
      setUsers(users);
    };

    const error = (err) => {
      console.log(err.message);
    };

    axios
      .get("https://dummyjson.com/users?limit=100")
      .then(success)
      .catch(error);
  };

  useEffect(getPosts, []);
  useEffect(getComments, []);
  useEffect(getUsers, []);

  const getUser = (id) => {
    let userData = '';
    const totalUser = users.length;

    for (let i = 0; i < totalUser; i++) {
      if (users[i].id === id) {
        userData = users[i];
        break;
      }
    }
    return userData.firstName + " " + userData.lastName;
  };

  return (
    <div className="container">
      {posts.map((post, idx) => {
        const userName = getUser(post.userId);
        const tags = [];
        post.tags.map((tag, idx) => {
          tags.push(tag + " ");
        });
        return(
          <Card idx={idx} post={post} comments={comments} userName={userName} tags={tags}/>
        )
      })}
    </div>
  );

  /*return (
    <div className="container">
      {posts.map((post, idx) => {
        const userName = getUser(post.userId);
        const tags = [];
        post.tags.map((tag, idx) => {
          tags.push(tag + " ");
        });
        return (
          <div className="card col-lg-6 text-left mt-5 ms-auto me-auto">
            <div className="card-header bg-info">
              <h4>{userName}</h4>
              <b>
                <span className="text-dark">Tags:</span>
                <span style={{ color: "maroon" }}> {tags}</span>
              </b>
            </div>
            <div className="card-body">
              <h5 className="card-title">{post.title}</h5>
              <p className="card-text">{post.body}</p>
            </div>
            <div className="card-footer text-light bg-dark">
              <b>Comments:</b>
            </div>
            <div className="card-body">
              {comments.map((comment, idx) => {
                if (post.id === comment.postId) {
                  return (
                    <p>
                      <strong>{comment.user.username}: </strong> {comment.body}
                    </p>
                  );
                }
              })}
            </div>
          </div>
        );
      })}
    </div>
  );*/
};

export default Posts;
