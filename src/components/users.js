import axios from "axios";
import React, { useState, useEffect } from "react";
import Table from "../common/table.component";
import _ from "lodash";

const Users = () => {
  // const columns = ["ID", "First Name", "Last Name", "Email", "User Name"];
  const [users, setUsers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);

  const getUsers = () => {
    const success = (response) => {
      const users = response.data.users;
      // console.log(users);
      setUsers(users);
    };

    const error = (err) => {
      console.log(err.message);
    };

    axios.get("https://dummyjson.com/users?limit=100").then(success).catch(error);
    // axios.get("https://dummyjson.com/users?limit=100&&skip=20").then(success).catch(error);
  };

  useEffect(getUsers, []);

  const columns = [
    {
      label: "ID",
      key: "id",
      isSortable: true,
    },
    {
      label: "First Name",
      key: "firstName",
      isSortable: true,
    },
    {
      label: "Last Name",
      key: "lastName",
      isSortable: true,
    },
    {
      label: "Email",
      key: "email",
      isSortable: true,
    },
    {
      label: "Username",
      key: "username",
      isSortable: true,
    },
    {
      label: "Action",
      render: () => {
        return (
          <td>
            <button className="btn btn-danger">Delete</button>
          </td>
        );
      },
    },
  ];

  const [sortKey, setSortKey] = useState("id");
  const [sortOrder, setSortOrder] = useState("asc");

  const handleSort = (newSortKey) => {
    if (sortKey === newSortKey) {
      if (sortOrder === "asc") {
        setSortOrder("desc");
      } else {
        setSortOrder("asc");
      }
    } else {
      setSortKey(newSortKey);
      setSortOrder("asc");
    }
  };

  const handlePageClick = (page) => {
    // console.log(page);
    setCurrentPage(page);
  }

  const sortedUsers = _.orderBy(users, sortKey, sortOrder);
  const paginatedUsers = _.slice(sortedUsers, ((currentPage-1) * 10), currentPage * 10);
  const pages = Math.ceil(sortedUsers.length / 10);
  const pageNumbers = [];

  for (let i = 1; i <= pages; i++) {
    // console.log({i, pages});
    pageNumbers.push(i);
  }

  console.log("============== ", currentPage);

  return (
    <div className="container mt-5">
      <Table
        items={paginatedUsers}
        columns={columns}
        sortKey={sortKey}
        sortOrder={sortOrder}
        handleSort={handleSort}
      />
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-end">
          <li className={currentPage === 1 ? "page-item disabled" : "page-item"} 
            onClick={() => { 
              if(currentPage-1 > 0) {
                handlePageClick(currentPage-1)
              }
            }}>
            <p className="page-link" tabIndex="-1" style={{cursor: "pointer"}}>
              Previous
            </p>
          </li>
          {pageNumbers.map(function (number, idx) {
            return(
            <li key={idx} className={currentPage === number ? "page-item active" : "page-item"} 
              onClick={() => handlePageClick(number)}>
              <p className="page-link" style={{cursor: "pointer"}}>
                {number}
              </p>
            </li>)
          })}
          <li className={currentPage === pages ? "page-item disabled" : "page-item"} 
            onClick={() => { 
              if(currentPage < pages) {
                handlePageClick(currentPage+1)
              }
            }}>
            <p className="page-link" style={{cursor: "pointer"}}>
              Next
            </p>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Users;
