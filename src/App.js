/*function App() {
  return (
    <>
      <h1>Home Page</h1>
    </>
  );
}*/

import React from "react";
import Counter from "./components/counter";
import Navbar from "./common/navbar";
import Login from "./components/login";
import Users from "./components/users";
import Todos from "./components/todos";
import Posts from "./components/posts";
import Register from "./components/register";
import { Route, Routes } from "react-router-dom";

class App extends React.Component {
  render() {
    return(
      <>
        <Navbar />
        <Routes>
          <Route path="/login" Component={Login} />
          <Route path="/register" Component={Register} />
          <Route path="/counter" Component={Counter} />
          <Route path="/users" Component={Users} />
          <Route path="/todos" Component={Todos} />
          <Route path="/posts" Component={Posts} />
        </Routes>
        {/* <Counter />
        <Counter />
        <Login /> */}
      </>
    );
  }
}

export default App;
